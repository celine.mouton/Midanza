---
layout: master/page
published: false
---
Site inspector
==============

<pre>
{{ site | inspect }}
</pre>

Debug info
----------

<pre>
{{ site | debug }}

</pre>