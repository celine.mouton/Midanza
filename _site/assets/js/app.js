$(function () {
   // console.log("Hello from jQuery");
    
    var $counters = $("[data-counter-start][data-counter-increment]");
    $counters.each(function(){
        var $counter = $(this); 
       // $counter.text("Dit is een counter"); 
        
        var count = $counter.data("counter-start"); 
        var increment = $counter.data("counter-increment"); 
        
        $counter.text(count.toLocaleString());

        setInterval(function () {
            count = count + increment; 
            $counter
                .css({
                    fontFamily:"monospace"
                })
                .text(count.toLocaleString()); 
        }, 100); 

    });

    var $scrollToTopButton = $("#scroll-to-top");
    var $body = $("body");
    var scrollTreshold = window.innerHeight /3; 
    $(window) 
    .on("scroll",function(){
        if($body.scrollTop()> scrollTreshold){
            $scrollToTopButton.show(); 
        }else {
            $scrollToTopButton.hide(); 
        }
    });
            $scrollToTopButton
                .on("click", function () {
                $("body").animate({
                    scrollTop:0
                })
            }); 

//Screenshot slider
var $slidesButtons = $(".slides-button"); //button 
var $slidesItems = $(".slides-item"); //tekst
var $screenItems = $(".screen-item"); //foto's

$slidesButtons
    .on("click", function(){
    var $activeSlidesButton = $(this);      
    var index = $activeSlidesButton.index(); 

    $slidesButtons
        .addClass("btn-secondary")
        .removeClass("btn-primary");

    $activeSlidesButton
        .addClass("btn-primary")
        .removeClass("btn-secondary");

    $slidesItems
        .hide()
        .eq(index).show();

    $screenItems
        .hide()
        .eq(index).show(); 

    })
    .first().trigger("click")
    
});